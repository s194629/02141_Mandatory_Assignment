

    open System
    open System.IO
    open System.Diagnostics
    open System.Collections.Generic
    open System.Text.RegularExpressions

    open FSharp.Text.Lexing
    open TypesAST
    open Parser
    open Lexer

    let graphInitials =
        "digraph program_graph {rankdir=LR;
        node [shape = circle]; q▷;
        node [shape = doublecircle]; q◀; 
        node [shape = circle]"
    
    let parse input =
        // Translate string into a buffer of characters
        let lexbuf = LexBuffer<char>.FromString input
        // Translate the buffer into a stream of tokens and parse them
        let res = Parser.start Lexer.tokenize lexbuf
        // Return the result of parsing (i.e. value of type "expr")
        res

    
    let compile input (nfa:bool) =
          
      let rec compileA inputA =
        match inputA with
        | Num(i) -> i.ToString()
        | Var(i) -> i
        | TimesExpr(i,k) -> sprintf "%s*%s" (compileA i) (compileA k) 
        | DivExpr(i,k) -> sprintf "%s/%s" (compileA i) (compileA k) 
        | PlusExpr(i,k) -> sprintf "%s+%s" (compileA i) (compileA k)
        | MinusExpr(i,k) -> sprintf "%s-%s" (compileA i) (compileA k)
        | PowExpr(i,k) -> sprintf "%s^(%s)" (compileA i) (compileA k)
        | UPlusExpr(i) -> sprintf "%s" (compileA i)
        | UMinusExpr(i) -> sprintf "-%s" (compileA i)
        | AccessArrIdxExpr(str, i) -> sprintf "%s[%s]" str (compileA i)
        | _ -> failwith "Cannot recognize input"
      
      let rec compileB inputB =
        match inputB with
        | TrueExpr(b) -> sprintf "true"
        | FalseExpr(b) -> sprintf "false"
        | GreaterExpr(a1, a2) -> sprintf "%s>%s" (compileA a1) (compileA a2)
        | GreaterEqualExpr(a1, a2) -> sprintf "%s>=%s" (compileA a1) (compileA a2)
        | LessExpr(a1, a2) -> sprintf "%s<%s" (compileA a1) (compileA a2)
        | LessEqualExpr(a1, a2) -> sprintf "%s<=%s" (compileA a1) (compileA a2)
        | EqualExpr(a1, a2) -> sprintf "%s=%s" (compileA a1) (compileA a2)
        | NotEqualExpr(a1, a2) -> sprintf "%s!=%s" (compileA a1) (compileA a2)
        | EagerOrExpr(b1, b2) -> sprintf "%s|%s" (compileB b1) (compileB b2)
        | ShortCircuitOrExpr(b1, b2) -> sprintf "%s||%s" (compileB b1) (compileB b2)
        | EagerAndExpr(b1, b2) -> sprintf "%s&%s" (compileB b1) (compileB b2)
        | ShortCircuitAndExpr(b1, b2) -> sprintf "%s&&%s" (compileB b1) (compileB b2)
        | NotExpr(b) -> sprintf "!%s" (compileB b)
        | _ -> failwith "Cannot recognize input"
      
      let rec compileC inputC firstState secondState noOfNewStates nfa =
        match inputC with
        | AssignToVariableExpr(s, i) -> sprintf "(%s, %s:=%s, %s); " firstState s (compileA i) secondState
        | AssignArrIdxExpr(s, i) -> sprintf "(%s, %s[%s]:=%s, %s); " firstState (fst s) (compileA (snd s)) (compileA i) secondState
        | IfCommandExpr(s) -> compileGC s firstState secondState noOfNewStates nfa
        | DoCommandExpr(s) -> compileDoGC s firstState secondState noOfNewStates nfa
        | LinkCommandExpr(c1, c2) -> (compileC c1 firstState (sprintf "q%d" (noOfNewStates+1)) (noOfNewStates+1) nfa) + (compileC c2 (sprintf "q%d" (noOfNewStates+1)) secondState (noOfNewStates+1) nfa)
        | SkipExpr -> sprintf "(%s, skip, %s); " firstState secondState
        | _ -> failwith "Cannot recognize input"
        // Return statement
      and compileGC inputGC firstState secondState noOfNewStates nfa =
        match inputGC with
        | GuardedCommandExpr(b, c) when nfa -> sprintf "(%s, %s, %s); " firstState (compileB b) (sprintf "q%d" (noOfNewStates+1)) + (compileC c (sprintf "q%d" (noOfNewStates+1)) secondState (noOfNewStates+1) nfa)
        | GuardedCommandExpr(b, c) when not nfa -> sprintf "(%s, (%s)&(!false), %s); " firstState (compileB b) (sprintf "q%d" (noOfNewStates+1)) + (compileC c (sprintf "q%d" (noOfNewStates+1)) secondState (noOfNewStates+1) nfa)
        | LinkGuardedCommands(gc1, gc2) -> (compileIfLinkedGC gc1 "" firstState secondState (noOfNewStates) nfa true) + (compileIfLinkedGC gc2 (getBooleanFromGc gc1) firstState secondState (noOfNewStates + calcDepthGC gc1) nfa false)
        | _ -> failwith "Cannot recognize input"
      and calcDepthGC inputGC =
        match inputGC with
        | GuardedCommandExpr(b, c) -> 1 + calcDepthC c
        | LinkGuardedCommands(gc1, gc2) -> calcDepthGC gc1 + calcDepthGC gc2
      and calcDepthC inputC =
        match inputC with
        | AssignToVariableExpr(s, i) -> 0
        | LinkCommandExpr(c1, c2) -> 1 + calcDepthC c1 + calcDepthC c2
        | IfCommandExpr(s) -> calcDepthGC s
        | _ -> 0
      and compileDoGC inputGC firstState secondState noOfNewStates nfa =
        match inputGC with
        | GuardedCommandExpr(b, c) when nfa -> compileC c firstState firstState noOfNewStates nfa + sprintf "(%s, !(%s), %s); " firstState (compileB b) secondState
        | GuardedCommandExpr(b, c) when not nfa -> compileC c firstState firstState noOfNewStates nfa + sprintf "(%s, !((%s)|false), %s); " firstState (compileB b) secondState
        | LinkGuardedCommands(gc1, gc2) when nfa -> (compileDoLinkedGC gc1 "" firstState noOfNewStates nfa true) + (compileDoLinkedGC gc2 "" firstState (noOfNewStates + calcDepthGC gc1) nfa) false + sprintf "(%s, %s, %s); " firstState (compileDoLinkedGCBools inputGC nfa false) secondState
        | LinkGuardedCommands(gc1, gc2) when not nfa -> (compileDoLinkedGC gc1 "" firstState noOfNewStates nfa true) + (compileDoLinkedGC gc2 (sprintf "(%s)" (getBooleanFromGc gc1)) firstState (noOfNewStates + calcDepthGC gc1) nfa false) + sprintf "(%s, !%s, %s); " firstState (compileDoLinkedGCBools inputGC nfa false) secondState
        | _ -> failwith "Cannot recognize input"
      and compileDoLinkedGC inputGC boolsOfPreviousLinks firstState noOfNewStates nfa isFirstLink =
        match inputGC with
        | GuardedCommandExpr(b, c) when nfa -> sprintf "(%s, %s, %s); " firstState (compileB b) (sprintf "q%d" (noOfNewStates+1)) + compileC c (sprintf "q%d" (noOfNewStates+1)) firstState (noOfNewStates+1) nfa
        | GuardedCommandExpr(b, c) when not nfa && not isFirstLink -> sprintf "(%s, (%s)&(!(%s|false)), %s); " firstState (compileB b) boolsOfPreviousLinks (sprintf "q%d" (noOfNewStates+1)) + compileC c (sprintf "q%d" (noOfNewStates+1)) firstState (noOfNewStates+1) nfa
        | GuardedCommandExpr(b, c) when not nfa && isFirstLink -> sprintf "(%s, (%s)&(!false), %s); " firstState (compileB b) (sprintf "q%d" (noOfNewStates+1)) + compileC c (sprintf "q%d" (noOfNewStates+1)) firstState (noOfNewStates+1) nfa
        | LinkGuardedCommands(gc1, gc2) when nfa -> (compileDoLinkedGC gc1 "" firstState noOfNewStates nfa isFirstLink) + (compileDoLinkedGC gc2 "" firstState (noOfNewStates + calcDepthGC gc1) nfa isFirstLink)
        | LinkGuardedCommands(gc1, gc2) when not nfa -> (compileDoLinkedGC gc1 boolsOfPreviousLinks firstState noOfNewStates nfa isFirstLink) + (compileDoLinkedGC gc2 (sprintf "(%s|%s)" boolsOfPreviousLinks (getBooleanFromGc gc1)) firstState (noOfNewStates + calcDepthGC gc1) nfa isFirstLink)
      and compileDoLinkedGCBools inputGC nfa isLast =
        match inputGC with
        | GuardedCommandExpr(b, c) when nfa -> sprintf "!(%s)" (compileB b)
        | GuardedCommandExpr(b, c) when (not nfa) && (not isLast) -> sprintf "(%s)" (compileB b)
        | GuardedCommandExpr(b, c) when (not nfa) && isLast -> sprintf "((%s)|false)" (compileB b)
        | LinkGuardedCommands(gc1, gc2) when nfa -> sprintf "(%s&%s)" (compileDoLinkedGCBools gc1 nfa false) (compileDoLinkedGCBools gc2 nfa false)
        | LinkGuardedCommands(gc1, gc2) when not nfa -> sprintf "(%s|%s)" (compileDoLinkedGCBools gc1 nfa false) (compileDoLinkedGCBools gc2 nfa true) // true in second call because it might be the last GC.
      and getBooleanFromGc inputGC =
        match inputGC with
        | GuardedCommandExpr(b, c) -> sprintf "%s" (compileB b)
      and compileIfLinkedGC inputGC boolsOfPreviousLinks firstState secondState noOfNewStates nfa isFirstLink =
        match inputGC with
        | GuardedCommandExpr(b, c) when nfa -> sprintf "(%s, %s, %s); " firstState (compileB b) (sprintf "q%d" (noOfNewStates+1)) + (compileC c (sprintf "q%d" (noOfNewStates+1)) secondState (noOfNewStates+1) nfa)
        | GuardedCommandExpr(b, c) when not nfa && not isFirstLink -> sprintf "(%s, (%s)&(!(%s|false)), %s); " firstState (compileB b) boolsOfPreviousLinks (sprintf "q%d" (noOfNewStates+1)) + compileC c (sprintf "q%d" (noOfNewStates+1)) secondState (noOfNewStates+1) nfa
        | GuardedCommandExpr(b, c) when not nfa && isFirstLink -> sprintf "(%s, (%s)&(!false), %s); " firstState (compileB b) (sprintf "q%d" (noOfNewStates+1)) + compileC c (sprintf "q%d" (noOfNewStates+1)) secondState (noOfNewStates+1) nfa
        | LinkGuardedCommands(gc1, gc2) when nfa -> (compileIfLinkedGC gc1 "" firstState secondState noOfNewStates nfa isFirstLink) + (compileIfLinkedGC gc2 "" firstState secondState (noOfNewStates + calcDepthGC gc1) nfa isFirstLink)
        | LinkGuardedCommands(gc1, gc2) when not nfa -> (compileIfLinkedGC gc1 boolsOfPreviousLinks firstState secondState noOfNewStates nfa isFirstLink) + (compileIfLinkedGC gc2 (sprintf "(%s|%s)" boolsOfPreviousLinks (getBooleanFromGc gc1)) firstState secondState (noOfNewStates + calcDepthGC gc1) nfa isFirstLink)                                                                                                                // If it is the last GC it will match "| GuardedCommandExpr(b, c) when (not nfa) && isLast"
                                                                                                                                                        // If it is not the last GC it will match "| LinkGuardedCommands(gc1, gc2) when not nfa"
      compileC input "q▷" "q◀" 0 nfa

    // Helper methods for interpreter
    
    let getValueOfVariable = new Dictionary<string, int array>()
    
    let securityOfVariables = new Dictionary<string, int>() // 0=public, 1=private

    let assignToVariable2 (input: string) = 
      match input with
      | str when Regex.IsMatch(str, @"\[(([0-9]+;)*[0-9]+)\]") -> (input.Substring(1,input.Length-2).Split(';')) |> Array.map(int)
      | str when Regex.IsMatch(str, @"[0-9]+") -> [|int input|]
      | _ -> failwith input

    let assignToVariable1 (input: string array) = printfn "%A" input; getValueOfVariable.Add(input.[0], assignToVariable2 input.[1])
    
    let rec listToValues (input:string list) =
      match input with
      | [] -> []
      | x::tail -> assignToVariable1 (x.Trim().Split '=') :: listToValues tail
    
    let setInitialValues (input:string) =
      input.Trim().Split ',' |> Array.toList
    
    // Interpreter

    let interpret input =
          
      let rec interpretA inputA =
        match inputA with
        | Num(i) -> i |> int
        | Var(i) -> getValueOfVariable.[i].[0]
        | TimesExpr(i,k) -> (interpretA i) * (interpretA k) 
        | DivExpr(i,k) -> (interpretA i) / (interpretA k) 
        | PlusExpr(i,k) -> (interpretA i) + (interpretA k)
        | MinusExpr(i,k) -> (interpretA i) - (interpretA k)
        | PowExpr(i,k) -> pown (interpretA i) (interpretA k)
        | UPlusExpr(i) -> (interpretA i)
        | UMinusExpr(i) -> - (interpretA i)
        | AccessArrIdxExpr(str,i) -> getValueOfVariable.[str].[interpretA i]
        | _ -> failwith "Cannot recognize input"
      
      let rec interpretB inputB =
        match inputB with
        | TrueExpr(b) -> true
        | FalseExpr(b) -> false
        | GreaterExpr(a1, a2) -> (interpretA a1) > (interpretA a2)
        | GreaterEqualExpr(a1, a2) -> (interpretA a1) >= (interpretA a2)
        | LessExpr(a1, a2) -> (interpretA a1) < (interpretA a2)
        | LessEqualExpr(a1, a2) -> (interpretA a1) <= (interpretA a2)
        | EqualExpr(a1, a2) -> (interpretA a1) = (interpretA a2)
        | NotEqualExpr(a1, a2) -> not((interpretA a1) = (interpretA a2))
        | EagerOrExpr(b1, b2) -> (interpretB b1) || (interpretB b2)
        | ShortCircuitOrExpr(b1, b2) -> (interpretB b1) || (interpretB b2)
        | EagerAndExpr(b1, b2) -> (interpretB b1) && (interpretB b2)
        | ShortCircuitAndExpr(b1, b2) -> (interpretB b1) && (interpretB b2)
        | NotExpr(b) -> not(interpretB b)
        | _ -> failwith "Cannot recognize input"
      
      let rec interpretC inputC anyFollowingGC =
        match inputC with
        | AssignToVariableExpr(s, i) -> getValueOfVariable.[s].[0] <- interpretA i
        | AssignArrIdxExpr(s, i) ->  getValueOfVariable.[(fst s)].[interpretA (snd s)] <- (interpretA i)
        | IfCommandExpr(s) -> interpretGC s anyFollowingGC
        | DoCommandExpr(s) -> while (getBooleanFromGc s) do (interpretDoGC s)
        | LinkCommandExpr(c1, c2) -> interpretC c1 true; interpretC c2 false
        | SkipExpr -> ()
        | _ -> failwith "Cannot recognize input"
        // Return statement
      and interpretGC inputGC anyFollowingGC =
        match inputGC with
        | GuardedCommandExpr(b, c) when interpretB b -> interpretC c anyFollowingGC
        | GuardedCommandExpr(b, c) when not (interpretB b) && not anyFollowingGC -> failwith ("Stuck: " + (sprintf "%A" getValueOfVariable))
        | GuardedCommandExpr(b, c) when not (interpretB b) && anyFollowingGC -> ()
        | LinkGuardedCommands(gc1, gc2) -> interpretGC gc1 true; interpretGC gc2 false
        | _ -> failwith "Cannot recognize input"
      and interpretDoGC inputGC =
        match inputGC with
        | GuardedCommandExpr(b, c) when interpretB b -> interpretC c true // true, for der er altid en node mere efter do-loop
        | GuardedCommandExpr(b, c) when not (interpretB b) -> ()
        | LinkGuardedCommands(gc1, gc2) -> interpretGC gc1 true; interpretGC gc2 true
        | _ -> failwith "Cannot recognize input"
      and getBooleanFromGc inputGC =
        match inputGC with
        | GuardedCommandExpr(b, c) -> interpretB b                                                                                                                                                // If it is not the last GC it will match "| LinkGuardedCommands(gc1, gc2) when not nfa"
      interpretC input false


  let security input =
     let rec securityA inputA =
        match inputA with
        | Num(i) -> i |> int
        | Var(i) -> getValueOfVariable.[i].[0]
        | TimesExpr(i,k) -> (interpretA i) * (interpretA k) 
        | DivExpr(i,k) -> (interpretA i) / (interpretA k) 
        | PlusExpr(i,k) -> (interpretA i) + (interpretA k)
        | MinusExpr(i,k) -> (interpretA i) - (interpretA k)
        | PowExpr(i,k) -> pown (interpretA i) (interpretA k)
        | UPlusExpr(i) -> (interpretA i)
        | UMinusExpr(i) -> - (interpretA i)
        | AccessArrIdxExpr(str,i) -> getValueOfVariable.[str].[interpretA i]
        | _ -> failwith "Cannot recognize input"
      
      let rec interpretB inputB =
        match inputB with
        | TrueExpr(b) -> true
        | FalseExpr(b) -> false
        | GreaterExpr(a1, a2) -> (interpretA a1) > (interpretA a2)
        | GreaterEqualExpr(a1, a2) -> (interpretA a1) >= (interpretA a2)
        | LessExpr(a1, a2) -> (interpretA a1) < (interpretA a2)
        | LessEqualExpr(a1, a2) -> (interpretA a1) <= (interpretA a2)
        | EqualExpr(a1, a2) -> (interpretA a1) = (interpretA a2)
        | NotEqualExpr(a1, a2) -> not((interpretA a1) = (interpretA a2))
        | EagerOrExpr(b1, b2) -> (interpretB b1) || (interpretB b2)
        | ShortCircuitOrExpr(b1, b2) -> (interpretB b1) || (interpretB b2)
        | EagerAndExpr(b1, b2) -> (interpretB b1) && (interpretB b2)
        | ShortCircuitAndExpr(b1, b2) -> (interpretB b1) && (interpretB b2)
        | NotExpr(b) -> not(interpretB b)
        | _ -> failwith "Cannot recognize input"
      
      let rec securityC inputC  =
        match inputC with
        | AssignToVariableExpr(s, i) when securityOfVariables.[s] = 1 -> "secure"
        | AssignToVariableExpr(s, i) when (securityA i) = 0 && securityOfVariables.[s] = 0 -> "secure"
        | AssignToVariableExpr(s, i) when (securityA i) > 0 && securityOfVariables.[s] = 0 -> "not secure"
        | AssignArrIdxExpr(s, i) when securityOfVariables.[(fst s)] = 1 -> "secure"
        | AssignArrIdxExpr(s, i) when (securityA i) = 0 && securityOfVariables.[(fst s)] = 0 -> "secure"
        | AssignArrIdxExpr(s, i) when (securityA i) > 0 && securityOfVariables.[(fst s)] = 0 -> "not secure"
        | IfCommandExpr(s) -> securityGC s
        | DoCommandExpr(s) -> while (getBooleanFromGc s) do (interpretDoGC s)
        | LinkCommandExpr(c1, c2) -> interpretC c1 true; interpretC c2 false
        | SkipExpr -> ()
        | _ -> failwith "Cannot recognize input"
        // Return statement
      and securityGC inputGC =
        match inputGC with
        | GuardedCommandExpr(b, c) when (securityB b) > 0 && (securityC c) = "not secure" ->
        | GuardedCommandExpr(b, c) when not (interpretB b) && not anyFollowingGC -> failwith ("Stuck: " + (sprintf "%A" getValueOfVariable))
        | GuardedCommandExpr(b, c) when not (interpretB b) && anyFollowingGC -> ()
        | LinkGuardedCommands(gc1, gc2) -> interpretGC gc1 true; interpretGC gc2 false
        | _ -> failwith "Cannot recognize input"
      and interpretDoGC inputGC =
        match inputGC with
        | GuardedCommandExpr(b, c) when interpretB b -> interpretC c true // true, for der er altid en node mere efter do-loop
        | GuardedCommandExpr(b, c) when not (interpretB b) -> ()
        | LinkGuardedCommands(gc1, gc2) -> interpretGC gc1 true; interpretGC gc2 true
        | _ -> failwith "Cannot recognize input"
      and getBooleanFromGc inputGC =
        match inputGC with
        | GuardedCommandExpr(b, c) -> interpretB b                                                                                                                                                // If it is not the last GC it will match "| LinkGuardedCommands(gc1, gc2) when not nfa"
      securityC input


    // Model checking

    type state(string Statename, string contense, state list nextStates)

    let states = new Dictionary<state list>()
    let visitedStates = new Dictionary<state list>()  

    let rec transitionTransform input =
      match input with
      | current::remaining when remaining.length > 0 -> transformToTransistion(current), transitionTransform(remaining)
      | current::remaining when remaining.length == 0 -> transformToTransistion(current)
      | _ -> failwith "Missing program graph"

    let transformToTransistion input =
      match input with
      | input -> states @ [input.contense, input.nextStates]

    let modelCheck input =
      match input with
      | input when input.length < 0 -> visitState(input)

    let rec visitState states =
      match states with
      | current::remaining when current.Equals("") && remaining.length > 0 -> visitedStates @ [current], visit(current), visitState(remaining)
      | current::remaining when current.Equals("Visited") && remaining.length > 0 -> visitState(remaining)
      | current::remaining when current.Equals("") && remaining.length > 0 -> visitedStates @ [current], visit(current)
      | current::remaining when current.Equals("Visited") && remaining.length > 0 -> return
      | _ -> failwith "Missing states"

    let visit input =
      match input with
      | input when input.nextStates.length == 0 -> return sprintf("Stuck at state: %s")
      | input when visitedStates.find(input) -> return sprintf("Infinite loop")

    // let signAnalyser input =
          
    //   let rec signAnalyserA inputA =
    //     match inputA with
    //     | Num(i) -> i |> int
    //     | Var(i) -> getValueOfVariable.[i]
    //     | TimesExpr(i,k) -> (signAnalyserA i) * (signAnalyserA k) 
    //     | DivExpr(i,k) -> (signAnalyserA i) / (signAnalyserA k) 
    //     | PlusExpr(i,k) -> (signAnalyserA i) + (signAnalyserA k)
    //     | MinusExpr(i,k) -> (signAnalyserA i) - (signAnalyserA k)
    //     | PowExpr(i,k) -> pown (signAnalyserA i) (signAnalyserA k)
    //     | UPlusExpr(i) -> (signAnalyserA i)
    //     | UMinusExpr(i) -> - (signAnalyserA i)
    //     | _ -> failwith "Cannot recognize input"
      
    //   let rec signAnalyserB inputB =
    //     match inputB with
    //     | TrueExpr(b) -> true
    //     | FalseExpr(b) -> false
    //     | GreaterExpr(a1, a2) -> (signAnalyserA a1) > (signAnalyserA a2)
    //     | GreaterEqualExpr(a1, a2) -> (signAnalyserA a1) >= (signAnalyserA a2)
    //     | LessExpr(a1, a2) -> (signAnalyserA a1) < (signAnalyserA a2)
    //     | LessEqualExpr(a1, a2) -> (signAnalyserA a1) <= (signAnalyserA a2)
    //     | EqualExpr(a1, a2) -> (signAnalyserA a1) = (signAnalyserA a2)
    //     | NotEqualExpr(a1, a2) -> not((signAnalyserA a1) = (signAnalyserA a2))
    //     | EagerOrExpr(b1, b2) -> (signAnalyserB b1) || (signAnalyserB b2)
    //     | ShortCircuitOrExpr(b1, b2) -> (signAnalyserB b1) || (signAnalyserB b2)
    //     | EagerAndExpr(b1, b2) -> (signAnalyserB b1) && (signAnalyserB b2)
    //     | ShortCircuitAndExpr(b1, b2) -> (signAnalyserB b1) && (signAnalyserB b2)
    //     | NotExpr(b) -> not(signAnalyserB b)
    //     | _ -> failwith "Cannot recognize input"
      
    //   let rec signAnalyserC inputC anyFollowingGC =
    //     match inputC with
    //     | AssignToVariableExpr(s, i) -> getValueOfVariable.[s] <- signAnalyserA i
    //     | AssignArrIdxExpr(s, i) ->  getValueOfVariable.[(fst s) + (signAnalyserA (snd s)).ToString()] <- (signAnalyserA i)
    //     | IfCommandExpr(s) -> signAnalyserGC s anyFollowingGC
    //     | DoCommandExpr(s) -> while (getBooleanFromGc s) do (signAnalyserDoGC s)
    //     | LinkCommandExpr(c1, c2) -> signAnalyserC c1 true; signAnalyserC c2 false
    //     | SkipExpr -> ()
    //     | _ -> failwith "Cannot recognize input"
    //     // Return statement
    //   and signAnalyserGC inputGC anyFollowingGC =
    //     match inputGC with
    //     | GuardedCommandExpr(b, c) when signAnalyserB b -> signAnalyserC c anyFollowingGC
    //     | GuardedCommandExpr(b, c) when not (signAnalyserB b) && not anyFollowingGC -> failwith ("Stuck: " + (sprintf "%A" getValueOfVariable))
    //     | GuardedCommandExpr(b, c) when not (signAnalyserB b) && anyFollowingGC -> ()
    //     | LinkGuardedCommands(gc1, gc2) -> signAnalyserGC gc1 true; signAnalyserGC gc2 false
    //     | _ -> failwith "Cannot recognize input"
    //   and signAnalyserDoGC inputGC =
    //     match inputGC with
    //     | GuardedCommandExpr(b, c) when signAnalyserB b -> signAnalyserC c true // true, for der er altid en node mere efter do-loop
    //     | GuardedCommandExpr(b, c) when not (signAnalyserB b) -> ()
    //     | LinkGuardedCommands(gc1, gc2) -> signAnalyserGC gc1 true; signAnalyserGC gc2 true
    //     | _ -> failwith "Cannot recognize input"
    //   and getBooleanFromGc inputGC =
    //     match inputGC with
    //     | GuardedCommandExpr(b, c) -> signAnalyserB b                                                                                                                                                // If it is not the last GC it will match "| LinkGuardedCommands(gc1, gc2) when not nfa"
    //   signAnalyserC input false

    // Other parts of the project

    let edgesArray (programGraph:string) =
      programGraph.Trim().Split ';' |> Array.toList
    
    let rec removeEmptyStrings (stringList:string list) =
      match stringList with
      | [] -> []
      | x::[] when isNull(x) || x.Trim() = "" -> []
      | x::[] -> x::[]
      | x::tail when isNull(x) || x.Trim() = "" -> removeEmptyStrings tail
      | x::tail -> x::(removeEmptyStrings tail)

    let edgeToPrettyPrint (edge:string[]) =
      edge.[0].Trim().Substring(1, edge.[0].Trim().Length-1) + " -> " +  edge.[2].Trim().Substring(0, edge.[2].Trim().Length-1) + " [label = \"" + edge.[1].Trim() + "\"];\n"

    let rec prettyPrinter (edgesArray:string list) =
      match edgesArray with
      | [] -> ""
      | x::[] -> edgeToPrettyPrint (x.Trim().Split ',')
      | x::tail -> edgeToPrettyPrint (x.Trim().Split ',')  + prettyPrinter tail
    
    let rec interpretResult (input:Dictionary<string, int[]>) =
      match input with
      | xs -> ()


    [<EntryPoint>]
    let main argv =
        
        printfn "Enter file names:"
        printfn "Note: The file extension .gv will be added."
        let fileName = Console.ReadLine() + ".gv"

        printfn "Initialize variables and values:"
        printfn "Example: x=1, y=0"
        let initialVariablesValues = Console.ReadLine()

        listToValues (setInitialValues initialVariablesValues)
    
        printfn "Create NFA or DFA? (type either \"NFA\" or \"DFA\" without \"\" and press enter)"
        let nfaOrDfaAnswer = Console.ReadLine()
        let isNfa = nfaOrDfaAnswer.ToUpper().Equals("NFA")

        printfn "Enter Guarded Commands program:"

        let parsedProgram = parse(Console.ReadLine())

        let compiledProgram = compile parsedProgram isNfa
        printfn "{ %s }" compiledProgram

        let content = graphInitials + "\n" + prettyPrinter (removeEmptyStrings (edgesArray compiledProgram)) + "}"

        File.WriteAllText(fileName, content)

        interpret parsedProgram
        // printfn "%A" getValueOfVariable

        printfn "Values of variables and arrays:"
        for entry in getValueOfVariable do
          printfn "%s: %A" entry.Key entry.Value

        printf "Compiled successful. Saved as: %s\n" fileName

        0