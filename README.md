## SGCParser (Simple Guarded Commands Parser)
This folder contains files for parsing a simple Guarded Commands program.

The project can also be found here: ```https://gitlab.gbar.dtu.dk/s194629/02141_Mandatory_Assignment.git```

The project can be run be writing ```dotnet run``` in a Terminal window.

The most important files are:
- ```Parser.fsp```
- ```Lexer.fsl```
- ```Grammar.g```
- ```TypesAST.fs```

For a "pretty printer", see the file:
- ```Program.fs```

Project by Gustav Nilsson Pedersen (s174562) and Thomas Monrad Laursen (s194629)