// This is how we specify the name of the grammar
grammar Grammar;

start : C EOF ;

a  :                    lhs = a '*' rhs = a  #TimesExpr
      |                 lhs = a '/' rhs = a  #DivExpr
      |                 lhs = a '+' rhs = a  #PlusExpr
      |                 lhs = a '-' rhs = a  #MinusExpr
      | <assoc=right>   lhs = a '^' rhs = a  #PowExpr
      |                 '(' a ')'            #NestedExpr
      | <assoc=right>   '+' e = a            #UPlusExpr
      | <assoc=right>   '-' e = a            #UMinusExpr
      |                 n = NUM              #NumExpr
      |                 x = VAR              #VarExpr
      ;

b  :                    lhs = a '>' rhs = a  #GreaterExpr     
      |                 lhs = a >= rhs = a   #GreaterEqualExpr
      |                 lhs = a '<' rhs = a  #LessExpr
      |                 lhs = a <= rhs = a   #LessEqualExpr
      |                 lhs = a '=' rhs = a  #EqualExpr
      |                 lhs = a != rhs = a   #NotEqualExpr
      |                 lhs = b '|' rhs = b  #EagerOrExpr
      |                 lhs = b || rhs = b   #ShortCircuitOrExpr
      |                 lhs = b '&' rhs = b  #EagerAndExpr
      |                 lhs = b && rhs = b   #ShortCircuitAndExpr
      | <assoc=right>   '!' e = b            #NotExpr
      |                 true = TRUE          #TrueExpr
      |                 false = FALSE        #FalseExpr
      ;

C  :    <assoc=right>   lhs = C ';' rhs = C           #LinkCommandExpr 
      |                 lhs = "if " GC rhs = " fi"    #IfCommandExpr
      |                 lhs = "do " GC rhs = " od"    #DoCommandExpr
      |                 skip = SKIP                   #SkipExpr                 
      ;

GC  :                   lhs = b -> rhs = C            #GuardedCommandExpr
      | <assoc=right>   lhs = GC [] rhs = GC          #LinkGuardedCommandExpr
      ;

NUM : ('0'..'9')+ ( '.' ('0'..'9')+)?  ('E' ('+'|'-')? ('0'..'9')+ )? ;
VAR : (('a'..'z')|('A'..'Z'))+ (('_')* (('a'..'z')|('A'..'Z'))* ('0'..'9')*)* ;
TRUE : true ;
FALSE : false ;

WS    : [ \t\r\n]+ -> skip ;
