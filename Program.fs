// A pretty printer based on the calculator

open FSharp.Text.Lexing
open System
open TypesAST
open Parser
open Lexer

let parse input =
    // Translate string into a buffer of characters
    let lexbuf = LexBuffer<char>.FromString input
    // Translate the buffer into a stream of tokens and parse them
    let res = Parser.start Lexer.tokenize lexbuf
    // Return the result of parsing (i.e. value of type "expr")
    res

// Function for user interaction
let rec compute n =
    if n = 0 then
        printfn "Bye bye"
    else
        printf "Enter a Guarded Commands program: "
        try
        // We parse the input string
        let e = parse (Console.ReadLine())
        // and print the result of evaluating it
        printfn "Result: %s" (e.ToString())
        compute n
        with err -> compute (n-1)

// Start interacting with the user
compute 3

[<EntryPoint>]
    let main argv =
        compute 3
        0