
module TypesAST  

type a =
  | Num of int
  | Var of string
  | TimesExpr of (a * a)
  | DivExpr of (a * a)
  | PlusExpr of (a * a)
  | MinusExpr of (a * a)
  | PowExpr of (a * a)
  | UPlusExpr of (a)
  | UMinusExpr of (a)
  | AccessArrIdxExpr of (string * a)
 

type b =
  | TrueExpr of bool
  | FalseExpr of bool
  | GreaterExpr of (a*a)
  | GreaterEqualExpr of (a*a)
  | LessExpr of (a*a)
  | LessEqualExpr of (a*a)
  | EqualExpr of (a*a)
  | NotEqualExpr of (a*a)
  | EagerOrExpr of (b*b)
  | ShortCircuitOrExpr of (b*b)
  | EagerAndExpr of (b*b)
  | ShortCircuitAndExpr of (b*b)
  | NotExpr of b

type GC =
  | GuardedCommandExpr of (b * C)
  | LinkGuardedCommands of (GC * GC)

and C =
  | SkipExpr
  | AssignToVariableExpr of (string * a)
  | AssignArrIdxExpr of ((string * a) * a)
  | LinkCommandExpr of (C * C)
  | IfCommandExpr of GC
  | DoCommandExpr of GC
